const evenNumbersInArray = (array) => {
    let result = [];
if(Object.keys(array).length === 0 || Array.isArray(array) == false)
{
    return "Passed argument is not an array or empty";
}
else
{
    for (let i = 0; i < array.length; i++) {
        if(array[i] % 2 == 0)
        {
            result.push(array[i]);
        }
    }
}
if(Object.keys(result).length === 0)
{
    return "Passed array does not contain even numbers";
}
return result;
};

module.exports = evenNumbersInArray;

